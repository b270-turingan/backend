// console.log("Hello World!");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program. They allow us to run statement /instruction if a condition is met or run another separate instruction if otherwise.


//  [SECTION] if, else if and else statement

// if statement - execute if a specified condition is true.
/*
Syntax:
if(condition) {
	statement
}
*/
	let numA = -1;

	if(numA < 0) {
		console.log("Hello!");
	}

	let city = "New York";

	if (city === "New York"){
		console.log("Welcome to New York City!");
	}

// else if statement
// Else if statement - executes a statement if previous conditions are false and if the specified condition is true.
	
	let numH = 1;

	if (numH < 0) {
		console.log("Hello Again!");
	}

	else if (numH > 0) {
		console.log("World");
	}

// else statement

	if (numA > 0){
		console.log ("Hello");
	}

	else if (numA === 0){
		console.log ("World");
	}

	else {
		console.log ("Again")
	}

// if else, else if and else statements with functions

	function determineTyphoonIntensity (windSpeed) {

		if(windSpeed < 30) {
			return "Not a typhoon yet.";
		}

		else if (windSpeed <= 61) {
			return "Tropical depression detected";
		}

		else if (windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical storm detected";
		} 

		else if (windSpeed >= 89 || windSpeed <=117) {
			return "Severe tropical storm detected";
		}

		else {
			return "Typhoon detected";
		}
	}

	// Returns the strings to the variable "message" that invoked it.
	let message = determineTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe tropical storm detected"){
		console.warn(message);
	}

// [SECTION] Truthy and Falsy
// Truthy Examples 

	if (true) {
		console.log("Truthy");
	}

	if (1) {
		console.log("Truthy");
	}

	if([]) {
		console.log ("Truthy");
	}

// Falsy Examples
	/*
		Falsy Values:
		1. ""
		2. null
		3. NaN
	*/
	
	if (false) {
		console.log("Falsy");
	}

	if(0) {
		console.log("Falsy");
	}

	if (undefined) {
		console.log("Falsy");
	}

// [SECTION] Conditional Ternary Operator

	/*
		Syntax:

		(expression) ? ifTrue : ifFalse;

		Conditional Ternary operator takes in three operands.
		1. Condition
		2. Expression to be executed if the condition is a truthy.
		3. Expression to be executed if the condition is a falsy.
	*/

	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);

// Another Example
	let name;
	function isOfLegalAge() {
		name = "John";
		return "You are of legal age limit";
	}

	function isUnderAge () {
		name = "Jane";
		return "You are under the age limit";
	}

// "parseInt()" converts the input received from the prompt into a number data type.
	let age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age > 17) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary operator in functions: " + legalAge + ", " + name);


// [SECTION] Switch Statement

/*
	Syntax:
	switch (expression){
	case value:
		statement;
		break;
	}

	default {
		statement;
	}
*/	

// The .toLowerCase() - will change the input received from the prompt into all lowercase ensuring a match with the switch case if the user inputs capitalized or uppercased.
	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {

		case "monday":
			console.log("The color of the day is red.");
			break;

	case "tuesday":
		console.log ("The color of the day is orange.");
		break;

	case "thursday":
		console.log("The color of the day is yellow.");
		break;

	case "wednesday":
		console.log ("The color of the day is green.");
		break;

	case "thursday":
		console.log("The color of the day is blue.");
		break;

	case "friday":
		console.log("The color of the day is indigo.");
		break;

	case "saturday":
		console.log("The color of the day is violet.");
		break;

	case "sunday":
		console.log("The color of the day is violet.");
		break;

	default:
		console.log("Please input a valid day");
	}	

// [SECTION] Try-Catch-Finally

	function showIntensityAlert (windSpeed){

		try {
			alerat(determineTyphoonIntensity(windSpeed));
		}

		catch (error) {
			console.log(typeof error);
			console.log(error.message);
			console.log(error.name);
		}

		finally {
			alert("Intensity updates will show new alert.");
		}
	}
	showIntensityAlert(56);
	console.log(typeof(showIntensityAlert));