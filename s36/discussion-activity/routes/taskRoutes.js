// Containes all the endpoints for our application.

const express = require("express");

// The "taskController" allows us to use the functions defined in the "taskController.js"
const taskController = require("../controllers/taskController")

// Creates a Router instance that functions as a middleware and routing system.
// Allows access to HTTP method middleswares that make is easier to create routes for our application.
const router = express.Router();

// [SECTION] Routes

// Route to get all the tasks.
router.get("/", (req, res) =>{

	// Invokes the "getAllTasks function from the "taskController.js" file
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send (resultFromController));
})

// ACTIVITY

// route for getting a specific task.
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id, req.body).then(resultFromController => res.send (resultFromController));
})

// route for changing the status of a task to "complete".
router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send (resultFromController));
})


// Export the router object to be used in the "index.js"
module.exports = router;