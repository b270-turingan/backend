// console.log("Hello World");

// Arithmetic Operators


// Addition
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

//  Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

// Division
	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

// Remainder
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operators

	// Basic Assignment Operator (=)
		let assignmentNumber = 8;

	// Addition Assignment Operator (+=) (the example below, is assigning the value)
	// 	assignmentNumber = assignmentNumber + 2;
	// 	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Short method -- Addition:
		assignmentNumber +=2;
		console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction:
		assignmentNumber -=2;
		console.log("Result of subtraction assignment operator: " + assignmentNumber);

	// Multiplication:
		assignmentNumber *=2;
		console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// Division:
		assignmentNumber /=2;
		console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators

		let number = 1 + 2 - 3 * 4 / 5;
		console.log ("Result of mdas operator: " + number);

		let pemdas = 1 + (2-3) * (4/5);
		console.log ("Result of pemdas operator: " + pemdas);

// Type Coercion
	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);	
// No Coercion
	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion);

// True value is 1 --- False value is 0.
	let numE = true + 1;
	console.log(numE);

// Comparison Operators
	let juan = "juan";

// Equality Operator (==)
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == "1"); //true
	console.log(0 == false); //true
	console.log("juan" == "juan"); //true
	console.log("juan" == juan); //true

// Inequality Operator
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != "1"); //false
	console.log(0 != false); //false
	console.log("juan" != "juan"); //false
	console.log("juan" != juan); //false

// Strict Equality Operator
	 console.log(1 === 1); //true
	 console.log(1 === 2); //false
	 console.log(1 === "1"); //false
	 console.log(0 === false); //false
	 console.log("juan" === "juan"); //true
	 console.log("juan" === juan); //true

// Strict Inequality Operator
	 console.log(1 !== 1); //false
	 console.log(1 !== 2); //true
	 console.log(1 === "1"); //true
	 console.log(0 !== false); //true
	 console.log("juan" !== "juan"); //false
	 console.log("juan" !== juan); //false

// Relational Operators (>)
	let a = 50;
	let b = 65;

// Greater than Operator
	console.log("Relational Operators")
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

// Less than Operator (<)
	let isLessThan = a < b;
	console.log(isLessThan); //true

// Greater than or Equal
	let isGtOrEqual = a >= b;
	console.log(isGtOrEqual); //false

// Less than or Equal
	let isLtOrEqual = a <= b;
	console.log(isLtOrEqual); //true

// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&&)
	let AllRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + AllRequirementsMet)

	// Logical OR Operator (||)
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (!)
	let someRequirementsNotMet = !isRegistered
	console.log("Result of logical NOT operator is: " + someRequirementsNotMet);

// Increment and Decremenet

	let z = 1;
// Pre-increment
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

// Post-increment
	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

// Pre-decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

// Post-decrement
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement:	" + z);