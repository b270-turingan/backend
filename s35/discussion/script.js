// Mongoose - ODM library that manages data relationship, validates schemas, and simplifies MongoDB document manipulation via the use of models.

// Schemas is a representation of a document's structure. It also containes a document's expected properties and data types.

// Models - a programming interface for querying or manipulating a database. A Mongoose model contains that simplify such operations.

const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

/*
	mongoose.connect - allows our application to be connected to MongoDB
	useNewUrlParser : true - allows us to avoid any current and future errors while connecting to MongoDB
	useUnifiedTopology : true - it allows us to conenct to MongoDB even if the required IP address is updated.
*/
mongoose.connect("mongodb+srv://admin:admin123@zuitt.iw3dl3o.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// connection to the database
// set notifications if the connection is a success or failure.
let db = mongoose.connection;
// if a connection error occu	red, output in the console.
// console.error.bind(console) allows us to print errors in the browser console as well as in the terminal
// "Connection error" is the message that will display if his happens.
db.once("error", console.error.bind(console, "connection error"));
// if the connection is successful, output in the console will display.
db.once("open", () => console.log('We are connected to the database'));


// [SECTION] Mongoose Schema
// Schemas determine the structure of documents to be written in the database; they act as blueprints of our data.
/*
	Use the Schema() constructor of the mongoose module to create a new Schema object.
*/
const taskSchema = new mongoose.Schema({
	// define the fields with corresponding data type
	// it needs a task "name" and task "status"
	// the "name" field requires  a String data types for its value
	name: String,
	// "status" field requires a String data type, but since we have default : "pending", users can leave this blank with a default value of "pending"
	status: {
		type: String,
		default: "pending"
	}
})

// [SECTION] Models
// Models use Schemas and they act as the middleman from the server to ouor database Server > Schema (blueprint) > Database > Colelction
// "Task" varibale can now be used to run commands for interacting with our database; the naming convention for mongoose models follows the MVC format
// The first parameter of the model() indicates the collection to store data that will be created.
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection.
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creating of todo list routes
// allows our app to read jason data
app.use(express.json());
// allows our app to read data from forms
app.use(express.urlencoded({extended:true}))

// Create a new task
/*
	Business Logic

	1. Check if the task is already existing in the collection. 
		- if it exists, return an error/notice.
		- if it is not, we add it the database.
	2. The task data will be coming from the request body.
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending"
*/

app.post('/task', (req, res) => {

	// checking for the duplicate tasks
	Task.findOne({name: req.body.name})
	.then((result, err) => {
		// if it exists, return an error/notice
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		} 
		// if no document was found
		else {
			// Create a new task and save it to the database.
			let newTask = new Task({
				name: req.body.name
			})
			// save the object in the collection.
			newTask.save()
			.then((saveTask, error) => {
				if (error){
					return console.error(error)
				} else {
					return res.status(201).send("New Task Created.")
				}
			})

		}
	})
})

// getting all tasks
/*
	Business Logic:
	1. Retrieve all the documents in the collections
	2. If an error is encountered, print the error
	3. If no errors are found, send a sucess (200), status back to the cliet/postman and return the array of document/result.
*/
app.get('/tasks', (req, res)=>{
	// retrieve all documents
	Task.find().then((result, err)=>{
		// error handling
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// ACTIVITY

// User Schema

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}
})

// Uer model

const Users = mongoose.model("Users", userSchema);
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// POST route


app.post('/signup', (req, res) => {
	Users.findOne({username: req.body.username, password: req.body.password})
	.then((result, err) => {
		if (result !== null && result.username === req.body.username && result.password === req.body.password){
			return res.send("User already exists");
		} else {
			let newUser = new Users ({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save()
			.then((saveUser, error) => {
				if (error){
					return console.error(error)
				}else {
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at ${port}`))

