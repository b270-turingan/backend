/*
Separation of Concerns:

What do we need to separate?
- Models
- Controllers
- Routes.

*/

// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoutes.js"
const taskRoutes = require("./routes/taskRoutes");

// Server setup
const app = express();
const port = 3001;

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.iw3dl3o.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Add the task route
// Allows all the task task routes created in the "taskRoute.js" file to use "/task" route
app.use("/tasks", taskRoutes)

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));