// console.log("Hello, Batch 270!");

// [SECTION] ES6 Updates
// ES6 is also known as ECMAScript 2015 is an update to the previous versions of ECMAScript
// ECMA - European Computer Manufacturers Association
// ECMAScript is the standard that is used to create implementation of the language, one which is JavaScript

// New Features of JavaScript

// [SECTION] Exponent Operator
// Using the exponent operator
const anotherNum = 8 ** 2;
console.log(anotherNum);


// Using Math object methods
const num = Math.pow(8, 2);
console.log(num);//64


// [SECTION] Template Literals

// Pre-Template Literals
// Uses single/double quotes(""/'')
let name = "Daisy";

let message = "Hello " + name + "! \nWelcome to programming!";
console.log(message);

// String Using Template Literals
// Uses backticks (``) instead of ('') or ("")
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// Multi-line Using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${anotherNum}.`
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript
	- "${}" are used to include the JS expressions.
*/

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructing
/*
	- Allows us to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers.
	- Syntax:
		let/const[variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = ["Jerico", "Paragas", "Turingan"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(`${firstName} ${lastName}`);
console.log(middleName);
console.log(lastName);
console.log(fullName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring

let person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}! It's good to see you.`)

let {givenName, midName, familyName} = person;
console.log(`Hello ${givenName} ${midName} ${familyName}! It's nice to meet you.`);

// [SECTION] Arrow Functions
/*
	- Compact alternative to traditional functions
	- This will only work with "function expression"
	- Syntax:
		let/const variableName = () => {
			//code block/statement
		}

*/

// fuction declaration
function greeting(){
	console.log("Hello World");
}
greeting();

// function expression
const greet = function(){
	console.log("Hi, World");
}
greet();

// Arrow Function
const greet1 = () => {
	console.log("Hello Again!");
}
greet1();

// Arrow Function with parameters
const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D.", "Smith");

// Arrow Function with Loops
const students = ["John", "Jane", "Judy"];

// Pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student`);
});

// Arrow Function
students.forEach((student)=>{
	console.log(`${student} is a student`);
})


// with return
// const add = (x,y) => {
// 	return x+y;
// }

// let total = add(1,2);
// console.log(total);

// Arrow Function - no return: Arrow function has an implicit value when the curly braces are omitted.

const add = (x,y) => x+y;
let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

const greet2 = (name = "User") => {
	return `Good Morning, ${name}!`;
}

console.log(greet2());
console.log(greet2("Jerico"));

// [SECTION] Class-Based Object Blueprints
/*
	- The "constructor" is a special method of class for creating/initializing an object for that class.
	- Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA
			this.objectPropertyB = objectPropertyB
		}
	}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Raptor";
myCar.year = 2021;
console.log(myCar);

// Instantiating a new object with initialized values
const myNewCar = new Car("Toyota", "Fortuner", "2020");
console.log(myNewCar);