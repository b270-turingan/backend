// Transfer data to HTTP
let http = require("http");

// Store the port number
const port = 4000;

// variable "server" that stores the output of the "createServer()" method.
let server = http.createServer((request, response) => {

	// Condition to request url using GET method.
	if (request.url == '/' && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to booking system")
	}

	else if (request.url == '/profile' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to your profile");
	}

	else if (request.url == '/courses' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available");
	}

	// Condition to request url using POST method
	else if (request.url == '/addCourse' && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add course to our resouces");
	}

	// Condition to request url using PUT method
	else if (request.url == '/updateCourse' && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update a course to our resouces");
	}

	// Condition to request url using DELETE method
	else if (request.url == '/archiveCourse' && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}

	// Default response if the request url is invalid.
	else {
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("404 Page Not Found")
	}
})

server.listen(port);
// When server is running, console will print the message to terminal
console.log(`Server is successfully running: ${port}`)