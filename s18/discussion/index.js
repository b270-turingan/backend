// console.log("Hello World");

// Function Parameters and Arguments

	// Last topic, we learned that we can gather data from user input using prompt (). Example:

	function printInput(){
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi " + nickname);
	}

	printInput();

	// Consider this function:

	// We can directly pass data inyo the function.
	function printName(name) {
		console.log("My name is " + name);
	}
	printName("Juana");

	// "name" - parameter.
	// A parameter acts a containers/named variable that exists only inside a function.

	// "Juana" - argument
	// An argument is the information/data provided directly into the function.
	
	// Variables can also be passed as an argument.
	let sampleVariable = "Yui";

	printName(sampleVariable);

	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log	("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	// Function as Arguments

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction) {
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

	// Multiple Parameters
	function createFullName(firstName, middleName, lastName) {
		console.log(firstName + " " + middleName + " " + lastName);
	}
	createFullName("Juan", "Dela", "Cruz");
	// In JS, providing more/less arguments than the expected parameters will not return an error.

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";
	createFullName(firstName, middleName, lastName);

	// Return Statement

	function returnFullName(firstName, middleName, lastName) {
		return firstName + " " + middleName + " " + lastName;

		// Any line/block of code that comes after the return statement is ignored because it end the funciton execution.
		console.log("This message will not be printed.");
	}

	let completeName = returnFullName("Jeff", "Smith", "Bezos");
	console.log(completeName);

	function returnAddress(city, country) {
		let fullAddress = city +" "+ country;
		return fullAddress;
	}
	let myAddress = returnAddress("Cebu City", "Philippines")
	console.log(myAddress);

	// Whatever value is returned from the returnFullName function is stored in the completeName variable.

	// Other sample
	// This funtion will only print the sum of num1 + num2
		function printSum (num1, num2) {
			return num1 + num2
		}

		function getProduct (num) {
			let product = num * 5
			console.log(product);
		}

		getProduct(printSum(5,5));

		function getDifference(num1, num2){
			return num1 - num2;
		}

		let difference = getDifference(10, 5);
		console.log(difference);

		function getQuotient(num){
			let quotient = num/2;
			console.log(quotient);
		}
		getQuotient(difference);