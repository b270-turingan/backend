alert ("Hello, Batch 270!");

// This is a statement.
	console.log("Hello again!");
	console.log("Hi, Batch 270")

// [SECTION] Variables
/* - this is used to contain data.*/

// Declarting Variables
// Syntaxt: let/const + varibaleName;

/*
	- Try to print out a value of a variable that has not been declared will return error of "undefined"
	- Variables must be declared first with value before they can ba used.
*/
	let myVariable; 
	console.log(myVariable);


	let greeting = "Hello";
	console.log(greeting);

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given its initial value
// Syntax: let/const variableName = value;
// let keyword is used if we want to reassign values to our varibles. Thus const keyword cannot reassign the value (it is fixed).
// Statements are instructions that we tell the computer to perform.
// Syntax are set of rules

	let productName = "desktop computer";
	productName = "Laptop";
	console.log(productName);

	let	productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539;
	console.log(interest);

	let friend = "Kate";
	console.log(friend);
	friend = 'Jay';
	console.log(friend);

// [SECTION] Data Types

// String - series of characters that create a word, a phrase, a sentence or anything related to creating a text. - can be written using either a single or double quote. Examples are below:
	let country = "Philippines";
	let province ="Batangas";
	console.log(country);
	console.log(province);

// Concatenating strings - Combining multiple values to create string using "+" symbol. 

	let fullAddress = province + ', ' + country;
	console.log(fullAddress);

	let address = 'I live in the ' + country;
	console.log(address);

	console.log("I live in the " + country);

// "\n" refers to creating a new line in between text
	let	mailAddress = 'Metro Manila\nPhilippines';
	console.log(mailAddress);

	let message = "John's employees went home early.";
	console.log(message);

// Numbers - Integers/Whole Numbers/Decimals/Exponential Notation
	let headCount = 26;
	console.log(headCount);


// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

// Boolean - value is either true or false.
	let isMarried = false;
	let inGoodConduct = true;
	console.log("isMarried" + isMarried);
	console.log("inGoodConduct " + inGoodConduct);

// Arrays - are special kind of data type that's used to store multiple - can store different data types ut is normally used to store similar data types. Syntax: let/const arrayName = [elementA, element B, ..]
	let grades = [98.7, 92.1, 90.2, 94.6]
	console.log(grades);

	let details = ["John", "Smith", 32, true];
	console.log(details);

// Objects
// Syntax : let/const objectName = {Property A: valueA, PropertyB: valueB ..}
	let person = {
		fullName: "Juan Dela Cruz", 
		age: 35,
		isMarried: false,
		contact: ["09186701522", "09183831673"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}
	console.log(person);

	let grade = {
		Math: 98.7,
		English: 92.1,
		Science: 90.2,
		Filipino: 94.6
	}

	console.log(grade);

// Null
	let gender = null;
	console.log(gender);