// console.log("Hello World!");

// Initialize trainer object by using literals
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
	        hoenn: ["May", "Max"],
	        kanto: ["Brock", "Misty"]
	    },
	talk: function() {
		console.log(this.pokemon[0] + "! I choose you!");
	}
}

// Object - Dot Notation
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);

// Object - square bracket notation
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Object - talk method.
console.log("Result of talk method:");
trainer.talk();

// Creating a constructor w/ following properties
function Pokemon(name, level) {
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level,
	// this.tackle - creates a function that if the attacker attacks it will reduce the health of target Pokemon. It also adds a condition that if the target Pokemon's health is <= 0, the target Porkemon will be fainted.
	this.tackle = function(targetPokemon) {
			targetPokemon.health -= this.attack;
			console.log(this.name + " tackled " + targetPokemon.name);
			console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
			if (targetPokemon.health <= 0){
				this.faint(targetPokemon);
			}
		};

		this.faint = function(targetPokemon) {
			console.log(targetPokemon.name + " fainted");
	
	}
}


let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);