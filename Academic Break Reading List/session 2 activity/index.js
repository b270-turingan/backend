// Activity 1 - Write a function to turn all elements of the following array into a string. socialMedia = ["Facebook", "Twitter", "Instagram", "Tiktok"]


	function arrayToStr(array) {
		let str = "";
		for (let i = 0; i < array.length; i++){
			str += array[i];
			if(i !== array.length - 1) {
				str += ","
			}
		}
		return str;
	}


	let socialMedia = ["Facebook", " Twitter", " Instagram", " Tiktok"];
	let strResult = arrayToStr(socialMedia);
	console.log(strResult);

// Activity 2 - Create a function that will add two Avengers in the following array. 1 at the beginning and 1 in between the other elements. avengers = ["Ironman", "Captain America". "Thor"]

let avengers = ["Ironman", "Captain America","Thor"];
let avenger1 = prompt("Please add your first Avenger");
let avenger2 = prompt("Please add your second Avenger");

function addAvengers(avenger1, avenger2){

	avengers.unshift(avenger1);
	avengers.splice(2, 0, avenger2);
	return avengers;
}

let finalAvengers = addAvengers(avenger1, avenger2);
console.log(finalAvengers);

// Activity 3 - Create an array of your friends' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.

let friends = ["Bryle", "Jericho", "Jethrove", "Charles"];
let removeAndAdd = prompt("Do you want to remove your last friend and add two friends? (yes or no)").toLowerCase();
	
	while(removeAndAdd !== "yes" && removeAndAdd !== "no"){
		removeAndAdd = prompt("Please answer Yes or No")
	}

let addFriend1;
let addFriend2;

function popAndPush (deleteLastFriend, addFriend1, addFriend2) {

	if(removeAndAdd === "yes") {
		deleteLastFriend.pop();
		addFriend1 = prompt("Add your first friend");
		addFriend2 = prompt("Add your second friend");
		deleteLastFriend.push(addFriend1, addFriend2)
	} else {
		alert("Thank you for confirming!")
	}
}

popAndPush(friends, addFriend1, addFriend2);
console.log(friends);
