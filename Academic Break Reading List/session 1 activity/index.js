// console.log("Hello World")

// [SECTION] JavaScript Repetition Control Structures

// Activity 1 - Using a for loop, change every second letter of a string to an uppercase 'O'

let name = prompt("Enter a Name or a word");
let newWord = stringName(name);
console.log(newWord);

	function stringName(name) {
	  let newName = "";
	  for (let i = 0; i < name.length; i++) {
	    if (i % 2 !== 0) {
	      newName += "O";
	    } else {
	      newName += name[i];
	    }
	  }
	  return newName;
	}

// Activity 2 - Write a while loop that generates a random number between 1 and 10 and prompts the user to guess the number. If the user guesses correctly, print a congratulatory message and exit the loop. If the user guesses incorrectly, give them a hint and prompt them to guess again.

let randomNumber = Math.floor(Math.random() *10) + 1;
let guessNumber = parseInt(prompt("Guess a number between 1 and 10"))
randomNumber === guessTheNumber(guessNumber);

function guessTheNumber (guessNumber){

	while(guessNumber !== randomNumber) {
		if (guessNumber < randomNumber) {
			guessNumber = parseInt(prompt("The number you enter is too low, Try Again!"))
		} else {
			guessNumber = parseInt(prompt("The number you enter is too high, Try Again"))
		}
	}
}
alert("Congratulations! Your guessed number " + randomNumber + " is correct!")
console.log("Congratulations you got the correct guessed!")

// Activity 3 - Write a do-while loop that prompts the user to enter a password until they enter the correct password. The correct password is "password123"

function logIn() {

	let password;
	do {
	  password = prompt("Please enter the correct Password");

	} while (password !== "password123");
	alert("You input the correct Password. Welcome to Zuitt!")
	console.log("Welcome to Zuitt!");
}

logIn();

