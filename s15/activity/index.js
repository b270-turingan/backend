console.log("Hello World");

// String

	let firstName = 'First Name: Roque Jerico';
	console.log(firstName);

	let lastName = 'Last Name: Turingan';
	console.log(lastName);

// Numbers
	
	let age = 23;
	console.log('Age: ' + age);

// Arrays
	
	let hobbies = ['Reading', 'Swimming', 'Watching Movies'];
	console.log("Hobbies:");
	console.log(hobbies);

// Objects

	let workAdd = {
		houseNumber: 167,
		street: "Marymount Street",
		city: 'City of Dasmariñas',
		state: 'Cavite'
	}
	console.log("Work Address:")
	console.log(workAdd);

	let name = "Steve Rogers";
		console.log("My full name is: " + name);

		let currentAge = 40;
		console.log("My current age is: " + currentAge);
		
		let friends = ["Tony","Bruce","Thor", "Natasha","Clint","Nick"];
		console.log("My Friends are: ")
		console.log(friends);

		let profile = {

			username: "captain_america",
			fullName: "Steve Rogers",
			age: 40,
			isActive: false,

		}
		console.log("My Full Profile: ")
		console.log(profile);

		let fullName = "Bucky Barnes";
		console.log("My bestfriend is: " + fullName);

		const lastLocation = "Arctic Ocean";
		console.log("I was found frozen in: " + lastLocation);