// console.log("Hello World");

// [SECTION] Arrays
// An Array in programming is simply a list of data.
// They are declares using the square brackets ([]) also known as "Array Literals".

let studentNumberA = "2023-1923";
let studentNumberB = "2023-1924";
let studentNumberC = "2023-1925";
let studentNumberD = "2023-1926";
let studentNumberE = "2023-1927";

let studentNumbers = ["2023-1923", "2023-1924", "2023-1925", "2023-1926" , "2023-1927"];
console.log(studentNumbers);

// Common examples of arrays
let grades = [71, 100, 85, 90];
console.log(grades);

let computerBrands = ["Acer", "Lenovo", "Dell", "Asus", "Apple", "Huawei"];
console.log(computerBrands);

// Alternative way to write arrays
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake bootstrap"
];
console.log(myTasks);

/*Creating ana array with values from variables:*/

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Nairobi";
let city4 = "Rio";

let cities = [city1, city2, city3, city4];
console.log(cities);

// [SECTION] .legth property

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

let fullName = "RJ Turingan";
console.log(fullName.length);

// length property can also set the total number of items in an array, meaning we can delete the last item in the array or shorten the array by simply updating the length property of the array

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities);

//Will not working with string
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// If we can shorten an array by setting the length property, we can also lengthen it by adding a number into the length property. However, it will be empty.

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);
theBeatles.length++;
console.log(theBeatles);

// [SECTION] Reading fromArrays

console.log(grades[0]);
console.log(computerBrands[3]);

// Accessing an array element that does not exist will return "undefined"
console.log(grades[20]);

let lakerLegends = ["Kobe", "Lebron", "Shaq", "Magic", "Kareem"];
console.log (lakerLegends[3]);

let currentLegends = lakerLegends[1];
console.log(currentLegends);

console.log("Array before reassignment");
console.log(lakerLegends);
lakerLegends[2] = "Davis";
console.log("Array after reassignment");
console.log(lakerLegends);

// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

/*
	 If you want to replace :
	 newArr[newArr.length-1] = "Barret Wallace";
	 console.log(newArr);
	 Output: ['Cloud Strife', 'Barret Wallace']

*/

// Looping over an Array
for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
}

// Another Example:
let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++) {
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
}

// [SECTION] Multi-dimensional Arrays

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
];
console.table(chessBoard);

console.log(chessBoard[1][4]); //e2
console.log(chessBoard[7][7]); //h8

console.log("Pawn moves to: " + chessBoard[1][5]);