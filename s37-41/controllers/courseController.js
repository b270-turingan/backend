const Course = require("../models/Course");
const auth = require("../auth");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/


/*
	ACTIVITY
	1. Refactor the course route to implement user authentication for the admin when creating a course.
	2. Refactor the addCourse controller method to implement admin authentication for creating a course.

*/
module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

			// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newCourse = new Course({

				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				slots: req.body.slots
			});

			if(true !== userData.isAdmin){
			return res.send({message: "Sorry, you are not allowed to use this feature"})
		} else {
			// Saves the created object to our database
			return newCourse.save().then(course => {
				console.log(course);
				res.send(true);
			})
			// Course creation failed
			.catch(error => {
				console.log(error);
				res.send(false);
			})
			
		}

	
}

// Retrieve all courses
/*
	Step:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(true !== userData.isAdmin){
			return res.send({message: "Sorry, you are not allowed to use this feature"})
	} else {
		return Course.find({}).then(result => res.send(result));
	}

	
}

// Retrieve all ACTIVE courses
/*
	Step:
	1. Retrieve all the courses from the database with property of "isActive: true"
*/
module.exports.getAllActive = (req, res) => {

	return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (req, res) => {

	console.log(req.params.courseId)

	return Course.findById(req.params.courseId).then(result => {
		console.log(result);
		return res.send(result);
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slot: req.body.slots
		}

		// Syntax: findByIdAndUpdate(documentId, updateCourse)
		// {new:true} - returns the updated document
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new:true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
		return res.send("You don't have access to this page.")
	}
}

// [ACTIVITY]
/*
	Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
*/

	module.exports.archiveCourse = (req, res) => {

		const userData = auth.decode(req.headers.authorization);

		if (userData.isAdmin){
			let archiveCourse = {
				isActive: req.body.isActive
			}
			return Course.findByIdAndUpdate(req.params.courseId, archiveCourse).then(result => {
				res.send(true);
			})
			.catch(error => {
				res.send(false);
			})
		} else {
			return res.send(false);
		}
	}