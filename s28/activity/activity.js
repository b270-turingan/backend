// Insert single room using insertOne method.

db.rooms.insertOne({
	name: "Single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// Insert multiple rooms using insertMany method

db.rooms.insertMany([
	{
		name: "Double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "Queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen size bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);

// Search a room named 'Double' using find method

db.rooms.find({name: "Double"});

// Use the updateOne method to update the queen room and set the availabe rooms to 0.

db.rooms.updateOne({
	name: "Queen"},
	{
		$set: {
			rooms_available: 0,
		}
	}
)

// Delete all rooms that have 0 availability.
db.rooms.deleteMany({rooms_available: 0});


