// Activity 1 - Create an object variable containing different properties of a product being sold. Write a function that returns a short description of the product using all its declared properties.

let productProperties = {
	name: prompt("Enter a Phone brand name"),
	color: prompt("Enter phone's color"),
	storage: prompt("Enter the phone's storage"),
	price: prompt("Enter the phone's price")
};

function productDescription(productProperties) {
	return "The " + productProperties.name + " is a " + productProperties.color + " Smart Phone with a storage of " + productProperties.storage + "." + "The Philippine Price of " + productProperties.name + " is " + "\u20B1" +productProperties.price; 
}

console.log(productDescription(productProperties));

// Activity 2 - Write a function that can add new properties to the created object in Activity 1 by assigning a value to a new property using dot or bracket notation. Display the object in the console.

function addNewProperty(product, propertyName, propertyValue) {
	// Using dot notation
	product[propertyName] = propertyValue;
	
	return product;
}

productProperties = addNewProperty(productProperties, "camera", "100 MP");
productProperties = addNewProperty(productProperties, "MegaPhone ratings", "4.0 stars");

console.log("Additional Properties of product: " + productProperties.name);
console.log(productProperties);


/* Activity 3
	Create a "person" object with the following properties:
	1. firstName: String
	2. lastName: String
	3. age: Number
	4. talk: function/method

	Using the "talk" method, let the "person" talk by printing the message "Hi, I am (full name) and I am (age) years old".
*/
let person = {
	firstName: prompt("Enter your First Name"),
	lastName: prompt("Enter your Last Name"),
	age: prompt("Enter your Age"),
	talk: function() {
		console.log("Hi, I am " + this.firstName + " " + this.lastName + " and I am " + this.age + " years old.");
	}
};
person.talk();

