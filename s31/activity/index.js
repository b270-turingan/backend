// Transfer data to HTTP
const http = require("http");

// Store the port number
const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to the login page");
	} else {
		response.writeHead(404,{'Content-Type': 'text/plain'});
		response.end("404 Page Not Found.")
	}
})

server.listen(port);
console.log(`Server is successfully running: ${port}`);