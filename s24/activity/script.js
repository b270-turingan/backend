// console.log("Hello World")

// Computing the cube of a number
const number = 2;
const getCube = 2 ** 3
console.log(`The cube of ${number} is ${getCube}`);

//  Creating a variable with a value of an array and destructure the array and print out the full address using Template Literals.
const address = ["258 Washington Ave", "NW, California", 90011];
const [street, state, zipCode] = address;
console.log(`I live at ${street} ${state} ${zipCode}`);

// Create a variable animal with it's details and destructure the object, print out a message of the animal using Template Literals.

const animal = {
	name: "Lolong",
	animalType: "salwater crocodile.",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

const{name, animalType, weight, measurement} = animal;
console.log (`${name} was a ${animalType} He weighed at ${weight} with a measurement of ${measurement}`);

// Creating an array of number, loop through forEach and printing out the numbers. Using reduced array method and an arrow function, it will console log the sum of all the numbers in an array.

const num = [1, 2, 3, 4, 5];
num.forEach((numbers) =>{
	console.log(numbers);
});

const reduceNumber = num.reduce((sum, numbers) => sum + numbers, 0);
console.log(reduceNumber);

// Creating a class of a Dog and a constructor that will accept name, age, and breed as it's properties. It will inistantiate a new object from class Dog and it will console the object.

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const dogProperties = new Dog("Frankie", 5, "Miniature Dachsund");
console.log(dogProperties);
