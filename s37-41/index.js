const express = require("express");
const mongoose = require	("mongoose");

// Allows our backend application to be available to our frontend.
const cors = require("cors");

// Allow access to routes defined within the application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.iw3dl3o.mongodb.net/b270_booking?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))




// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Defines the "/users" and "/courses" to be included for all user and course routes
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);



// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});