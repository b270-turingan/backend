// Count the total numbers of fruits on sale

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}

]);

// Count the total number of fruits with stock more than or equal to  20.

db.fruits.aggregate([

	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}

]);

// get the average price of fruits onSale per supplier.

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}

]);

// get the highest price of a fruit per supplier.

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}

]);

//  Get the lowest price of a fruit on Sale per supplier.

db.fruits.aggregate([

	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	
]);