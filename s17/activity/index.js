// console.log("Hello World");

// Welcome Message Using Prompt
	// Start of Welcome Message

	function welcomeMessage () {
		let fullName = prompt("What is your name?");
		let age = prompt ("How old are you?");
		let address = prompt("Where do you live?");
		alert("Thank you for your input!");

		console.log("Hello, " + fullName);
		console.log("you are " + age + " years old.");
		console.log("You live in " + address);
	}

	welcomeMessage ();
	// End of Welcome Message

// Display Favorite Band/Musical Artist
	// Start of Band/Musical Artist

	function printArtist () {
		let artistA = ("Bamboo");
		let artistB = ("Parokya Ni Edgar");
		let artistC = ("The Script");
		let artistD = ("Paramore");
		let artistE = ("Metallica")

		console.log("1." + artistA);
		console.log("2. "+ artistB);
		console.log("3. "+ artistC);
		console.log("4. "+ artistD);
		console.log("5. "+ artistE);
	}

	printArtist();
	// End of Band/Musical Artist

// Display of Favorite Movies with Tomatoes rating.
	// Start of Favorite Movies

	function printMovies() {
		let movieA = ("Fatherhood");
		let movieB = ("Instant Family");
		let movieC = ("Pk");
		let movieD = ("Gunjan Saxena: The Kargil Girl");
		let movieE = ("All Quiet on the Western Front");
		let movieRating = ("Rotten Tomatoes Rating: ");

		console.log("1. " + movieA);
		console.log(movieRating + "67%");
		console.log("2. " + movieB);
		console.log(movieRating + "81%");
		console.log("3. " + movieC);
		console.log(movieRating + "78%");
		console.log("4. " + movieD);
		console.log(movieRating + "100%");
		console.log("5. " + movieE);
		console.log(movieRating + "89%");
	}

	printMovies();

	// End of Favorite Movies

// Debugging Practice
	// Start of Debugging Practice

		function printFriends () { //= function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); //alert
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 	
	};


	printFriends();
	// End of Debugging Practice