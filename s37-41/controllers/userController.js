const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth.js");
// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method
*/
module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if (result.length > 0) {
			return res.send (true);
		// No duplicate email found
		// The user is not yet registered in the database.
		} else {
			return res.send(false);
		}
	})
	.catch(error => res.send (error));
}

// // User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {

	// Creates a variable "newUser" and instantiats a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information.
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,

		// Syntax: bcrypt.hashSync(dataToBeEncrypted, saltRounds)
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Saves the created objects to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		// User does not exist
		if(result == null) {

			return res.send({message: "No user found"});
		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
				// Generate an access token by invoking the "createAccessToken" in the auth.js file
				return res.send({accessToken: auth.createAccessToken(result)});

			// Password do not match
			} else {
				return res.send({message: "Incorrect Password"})
			}
		}
	})
}

// [ACTIVITY]

/*
	Create a getProfile controller method for retrieving the details of the user:
	    - Find the document in the database using the user's ID
	    - Reassign the password of the returned document to an empty string
	    - Return the result back to the postman
*/

module.exports.getProfile = (req, res) => {

		const userData = auth.decode(req.headers.authorization);
		console.log(userData);

		return User.findById(userData.id).then(result=> {

		//Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

		result.password = "";

		// Returns the user information with the password as an empty string
		return res.send(result);
		})
	
}

// Another code:
	/*return User.findById(req.body.id).then(result => {
		// User does not exists
		if(result == null){
			return res.send({message: "No user found"});

		// User exists
		} else {
			let userDetails = result.toObject();
			userDetails.password = "";
			return res.send(userDetails);
		}
	})
	.catch(error => res.send(error));*/

// Enroll user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Database

	Workflow:
	User logs in, server will respond with JWT on successful authentication
	A POST request with the JWT in its header and the course ID in its request body will be sent to the /users/enroll endpoint
	Server validates JWT
	If valid, user will be enrolled in the course
	If invalid, deny request
*/

module.exports.enroll = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// To retrieve the course name
	let courseName = await Course.findById(req.body.courseId).then(result => result.name);

	// User ID and email will be retrieved from the payload/token
	let data = {
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseId: req.body.courseId,
		courseName: courseName
	}
	console.log(data);

	if(userData.isAdmin){
		return res.send(false)
	} else {
			let isUserUpdated = await User.findById(data.userId).then(user => {

			user.enrollments.push({
				courseId: data.courseId,
				courseName: data.courseName
			});

			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({
				userId: data.userId
			})
			// Minus the slots available by 1
			course.slots -= 1;
		return course.save().then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
		});


		console.log(isCourseUpdated);

		if(isUserUpdated && isCourseUpdated) {
			return res.send(true);
		} else {
			return res.send(false);
		}
	}

		
}